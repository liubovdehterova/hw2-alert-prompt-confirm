/*
Які існують типи даних у Javascript?
* 1. String
* 2. Number
* 3. Boolean
* 4. undefined
* 5. null
* 6. BigInt
* 7. Object
* 8. Symbol
* */

/*
У чому різниця між == і ===?
* Оператор == (нестрога рівність) порівнює два операнди з врахуванням автоматичного приведення типів
Перед порівнянням JavaScript спробує перетворити операнди на один тип даних, якщо вони мають різні типи.
* Оператор === (строга рівність) порівнює два операнди без приведення типів.
Він повертає true лише тоді, коли типи обох операндів і їх значення є ідентичними.
/*
Що таке оператор?
* Оператор - це символи, які вказують на виконання певних операцій над одним або більше операндами для отримання результату.
* Арифметичні оператори: '+', '-', '/', '*', '%'
* Оператори порівняння: '==', '===', '!=', '!==', '<', '>', '<=', '>='
* Оператори присвоєння: '=', '+=', '-=', '*=', '/='
* Логічні оператори: '&&', '||', '!'
* */

let userName = prompt("Введіть ім'я");
let userAge = prompt('Введіть вік');

while (!userName && !userAge || isNaN(userAge)) {
    alert('Будь ласка, введіть коректні дані.');
    userName = prompt("Введіть ім'я", userName);
    userAge = prompt('Введіть вік(має бути число)', userAge);
}

if (+userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (+userAge >= 18 && +userAge <= 22) {
    let answer = confirm('Are you sure you want to continue?');
    if (answer) {
        alert('Welcome, ' + userName);
    } else {
        alert('You are not allowed to visit this website');
    }
} else if (userAge > 22) {
    alert('Welcome, ' + userName);
}


